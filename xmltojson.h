#include<QtXml>
#include<QtCore>
#include<QFile>
#include<QXmlSimpleReader>
#include<QDomComment>
#include<list>
#include<sstream>
#include"serializer.h"
#include"Form.h"
#ifndef XMLTOJSON_H
#define XMLTOJSON_H


#define DEBUG

/**
    This Program will parse a Pencil xml format, to a json Readible way.
*/
class XmlToJson
{
public:
    XmlToJson();
    XmlToJson(QString filename,QString outFilename = "",QString customRoot = "");
    void parse();
    QVariant getParsed(const QString str, const QString rootElement = "");
    QVariant readElement(const QDomElement &element, QString padd = "");
    QVariant readDocument(const QDomDocument &doc,QString name = "",bool multipleByRoot = false);
    void toJson(QString inputFile,QString outPutFile,QString customRoot = "",bool multipleByRootName = false);

    void printVariant(QVariant var);

    //Check for document Validation
    //Selecting Nodes From QDomNode
    QVariant parseAttributes(QDomNode node);
    QVariant parseProperty(const QDomNode node);
    QVariant parseValue( QString var);
    bool isListRoot(QDomElement&);
    bool checkFileExistance(const QString filename);
    bool validateInputs();
    bool validateDocument(QFile*,int*,int*,QString* msg = NULL,QDomDocument* doc = NULL);
private:
    QString filename;
    QString outputRoute;
    QString customRootName;//for finding especific nodes and parsing
    bool multipleCustomRoot;//for parsing multiples nodes with the same name
    bool removeElements;
};

#endif // XMLTOJSON_H
