#include"qobjecthelper.h"

#ifndef FORM_H
#define FORM_H

class Form: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName)

public:
    Form(QObject* parent = 0){}
    ~Form(){};

    QString name()const { return m_name;}
    void setName(const QString& name)
    {
        m_name = name;
    }
private:
    QString m_name;
};


#endif // FORM_H
