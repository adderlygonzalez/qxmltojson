#-------------------------------------------------
#
# Project created by QtCreator 2013-04-15T17:47:37
#
#-------------------------------------------------

QT       += core \
            xml

QT       -= gui

LIBS += -L$$PWD/lib -lqjson

TARGET = xmltojson
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += $$PWD/lib/Headers \

SOURCES += main.cpp \
    xmltojson.cpp

HEADERS += \
    xmltojson.h \
    Form.h \
    snippets.h

RESOURCES += \
    resources.qrc

OTHER_FILES +=
