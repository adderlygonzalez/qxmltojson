#include "xmltojson.h"

XmlToJson::XmlToJson()
{
}


XmlToJson::XmlToJson(QString inFile,QString outFile,QString customRoot)
{
    this->filename  = inFile;
    this->outputRoute = outFile;
    this->customRootName = customRoot;
}

void XmlToJson::toJson(QString inputFile,QString outPutFile,QString customRootName,bool multipleByRootName)
{
    this->filename = inputFile;
    this->outputRoute = outPutFile;
    this->customRootName = customRootName;
    this->multipleCustomRoot = multipleByRootName;

    bool error = false;
    QString errorMessage;
    QFile outfile(outPutFile);

    if(!validateInputs())
        error = true;

    if(!outfile.open(QFile::WriteOnly|QFile::Truncate)){
        errorMessage.append("Could not create output source ");
        error = true;
    }
     if(error) {
         qDebug() << errorMessage ;
         return ;
     }

    QVariant parsed = getParsed(this->filename,this->customRootName);
    QJson::Serializer serializer;
    serializer.setIndentMode(QJson::IndentFull);
    serializer.allowSpecialNumbers(true);

    bool ok;
    QByteArray json = serializer.serialize(parsed,&ok);

    if(ok){
        qDebug() << json;
    }else{
        qDebug() << "Something went wrong "<<serializer.errorMessage();
        return;
    }
    outfile.write(json);
    outfile.close();
}

void XmlToJson::printVariant(QVariant var)
{
    QJson::Serializer serializer;
    serializer.setIndentMode(QJson::IndentFull);
    serializer.allowSpecialNumbers(true);

    bool ok;
    QByteArray json = serializer.serialize(var,&ok);
    if(ok){
        qDebug() << "LIST" << json;
    }else{
        qDebug() << "Something went wrong "<<serializer.errorMessage();
    }
}
void XmlToJson::parse()
{
    bool error = false;
    QString errorMessage;
    QFile outfile(this->outputRoute);

    if(!validateInputs())
        error = true;

    if(!outfile.open(QFile::WriteOnly|QFile::Append|QFile::Truncate)){
        errorMessage.append("Could not create output source ");
        error = true;
    }
     if(error) {
         qDebug() << errorMessage ;
         return ;
     }

    QVariant parsed = getParsed(this->filename,this->customRootName);
    QJson::Serializer serializer;
    serializer.setIndentMode(QJson::IndentFull);
    serializer.allowSpecialNumbers(true);

    bool ok;
    QByteArray json = serializer.serialize(parsed,&ok);

    if(ok){
        qDebug() << json;
    }else{
        qDebug() << "Something went wrong "<<serializer.errorMessage();
        return;
    }
    outfile.write(json);
    outfile.write("\n");
    outfile.close();
}
QVariant XmlToJson::getParsed(const QString str, const QString rootElement)
{    
    QVariant doc;//result map
    int errorLine, errorColumn;
    QString errorMsg;
    QFile* file = new QFile(str);
    QDomDocument document;

    if(!validateDocument(file,&errorLine,&errorColumn,&errorMsg,&document)){
        QVariantMap map;
        map.insert("message","Problem Parsing the document");
        map.insert("errorLine",errorLine);
        map.insert("errorColumn",errorColumn);
        map.insert("errorMessage",errorMsg);
#ifdef DEBUG
        qDebug() << map;
#endif
        return (QVariant)map;
    }
    doc = readDocument(document,rootElement,this->multipleCustomRoot);
    file->close();
    return doc;
}

QVariant XmlToJson::readDocument(const QDomDocument &doc,QString rootName ,bool multipleByRoot)
{
    QVariantMap docMap;
    bool checkForRootName = !rootName.isEmpty();

    if(checkForRootName)
    {
       QDomNodeList listOfRoot = doc.elementsByTagName(rootName);
       qDebug() << "count" << listOfRoot.count()<< "Doc"  << rootName << "__" << (listOfRoot.isEmpty() ? "IS EMPTY" : "NOT EMPTY") ;
       if(!listOfRoot.isEmpty())
       {
           QDomNode tmpNode;
           if(multipleByRoot && listOfRoot.size() > 1)
           {
               QVariantList multipleList;
               for(int rootNodes = 0; rootNodes < listOfRoot.size();rootNodes++)
               {
                   tmpNode = listOfRoot.item(rootNodes);
                   multipleList.push_back(readElement(tmpNode.toElement()));
               }
               docMap.insert(rootName,multipleList);
           }else{
                 docMap.insert(rootName,readElement(listOfRoot.at(0).toElement()));
           }
       }else{
           docMap.insert("message",rootName.append(" Not Found "));
       }
    }else{
        QVariant childNode;
        QString childName;
        for(QDomNode node = doc.firstChild(); !node.isNull(); node = node.nextSibling()){
            QDomElement tmpEl =node.toElement();
           childName = tmpEl.tagName();
           childNode = readElement(tmpEl);
           docMap.insert(childName,childNode);
        }
    }
    return docMap;
}
QVariant XmlToJson::readElement(const QDomElement &element, QString paddVal)
{
    QVariantMap map;//ElementMap

    //Check Duplicate nodes -> to Make a list
    QMap<QString,int> duplicatesNodes;
    for(QDomNode node = element.firstChild();!node.isNull();node = node.nextSibling())
    {
        QString _nodeName = node.nodeName();

        if(node.nodeType() == node.TextNode) break;//hack for textnodes

        //increased the amount of actualNodename
        if(duplicatesNodes.contains(_nodeName)){
            int dupVal = duplicatesNodes.value(_nodeName);
            dupVal++;
            duplicatesNodes.insert(_nodeName,dupVal);
        }else{
            //insert 1 for the first element of this found
            duplicatesNodes.insert(_nodeName,1);
        }
    }

    //Parsing duplicates and single
    for(QMap<QString,int>::Iterator it = duplicatesNodes.begin();it != duplicatesNodes.end();it++)
    {
        int amount = it.value();
        QString nodeName = it.key();

        QVariantList dupAttrs ;
        QDomNodeList nodes = element.elementsByTagName(nodeName);

        //if amount > 1 means it should be a listOf nodes
        if( amount > 1)
        {
            for(int dupAmount = 0;dupAmount < nodes.size();dupAmount++)
            {
                QDomNode _node = nodes.at(dupAmount);
                dupAttrs.push_back(readElement(_node.toElement()));
            }
            map.insert(nodeName,dupAttrs);
        }else{
            map.insert(nodeName,readElement(nodes.at(0).toElement()));

        }
    }

    /*
        Parse attributes of the passed element  .
    */
    if(element.hasAttributes())
    {
        QVariantMap nodeAttr = parseAttributes(element).toMap();
        for(QVariantMap::Iterator it = nodeAttr.begin();it != nodeAttr.end();it++)
            map.insert(it.key(),it.value());
    }

    return map;
}

QVariant XmlToJson::parseAttributes(QDomNode node)
{
    QVariantMap map;

    node.normalize();
    map.insert("value",node.toElement().text());
    QDomNamedNodeMap attrs =  node.attributes();
    for(int x = 0;x < attrs.size();x++)
    {
        QDomNode tmpNode = attrs.item(x);
        QVariant realNodeVal(parseProperty(tmpNode));

        switch(realNodeVal.type())
        {
        case QVariant::Double:
             map.insert(tmpNode.nodeName(),realNodeVal.toDouble());
            break;
        case QVariant::Int:
            map.insert(tmpNode.nodeName(),realNodeVal.toInt());
            break;
        case QVariant::UInt:
            map.insert(tmpNode.nodeName(),realNodeVal.toUInt());
            break;
        case QVariant::LongLong:
            map.insert(tmpNode.nodeName(),realNodeVal.toLongLong());
            break;
        case QVariant::Hash:
            map.insert(tmpNode.nodeName(),realNodeVal.toHash());
            break;
        case QVariant::Line:
            map.insert(tmpNode.nodeName(),realNodeVal.toInt());
            break;
        case QVariant::String:
            map.insert(tmpNode.nodeName(),realNodeVal.toString());
            break;
        case QVariant::TextFormat:
            map.insert(tmpNode.nodeName(),realNodeVal.toString());
            break;
        case QVariant::Font:
            map.insert(tmpNode.nodeName(),realNodeVal.toString());
            break;
        case QVariant::UserType:{
                map.insert(tmpNode.nodeName(),realNodeVal.toString());
                qDebug() << " USERTYPE nodeType = " << tmpNode.nodeType() << " Val = "<<tmpNode.nodeValue() ;
            }
            break;
        default:{
                qDebug() << " Unidentified nodeType = " << tmpNode.nodeType() << " Val = "<<tmpNode.nodeValue() ;
                map.insert(tmpNode.nodeName(),tmpNode.nodeValue());
            }
            break;
        }
    }
    return map;
}

/*
  TODO:add more types of nodes so theres no need for the patch done
    in readElement...  if(node.nodeType() == node.TextNode)
    Parse The Properties, depenting the type of properties
*/
QVariant XmlToJson::parseProperty(const QDomNode node)
{
    if(node.isNull())
        return QVariant();

    QString name;
    QString value;

    if(node.isAttr()){
        QDomAttr attr = node.toAttr();
        name = attr.nodeName();
        value = attr.value();
    }else if(node.isCDATASection()){
        QDomCDATASection cdata = node.toCDATASection();
        name = cdata.nodeName();
        value = cdata.nodeValue();
    }else if(node.isCharacterData()){
        QDomCharacterData charr = node.toCharacterData();
        name = charr.nodeName();
        value = charr.nodeValue();
    }else if(node.isComment()){
        QDomComment comment = node.toComment();
        name = comment.nodeName();
        value = comment.nodeValue();
    }else if(node.isDocumentType()){
        QDomDocumentType doctype = node.toDocumentType();
        name = doctype.nodeName();
        value = doctype.nodeValue();
    }else if(node.isElement()){
        QDomElement element = node.toElement();
        name = element.nodeName();
        value = element.nodeValue();
    }else if(node.isText()){
        QDomText text = node.toText();
        name = text.nodeName();
        value= text.nodeValue();
    }
    return parseValue(value);
}

QVariant XmlToJson::parseValue(QString var)
{
    QVariant returnval;
    int n;
    double d;
    std::string str;
    std::stringstream ss(var.toStdString().c_str());


    if((ss >> d ) && !ss.fail()){
        returnval.setValue(d);
        return returnval;
    }
    ss.clear();
    if((ss >> n) && !ss.fail() ){
        returnval.setValue(n);
        return returnval;
    }
    ss.clear();
    if((ss >> str) && !ss.fail()){
        returnval.setValue(QString(ss.str().c_str()));
        return returnval;
    }
    ss.clear();
    returnval.setValue(QString(ss.str().c_str()));
    return returnval;
}

bool XmlToJson::validateDocument(QFile *file, int *errorLine, int *errorColumn, QString *errorMsg,QDomDocument* doc)
{
    QDomDocument *document;
    if(doc != NULL)
        document = doc;
    else document = new QDomDocument;

    if(!document->setContent(file,errorMsg,errorLine,errorColumn))
    {
        QString error("Syntax error line %1, column %2:\n%3");
                    error = error
                            .arg(*errorLine)
                            .arg(*errorColumn)
                            .arg(*errorMsg);
                    qDebug() << error;
                    if(errorMsg != NULL){
                        *errorMsg = error;
                    }
                    return false;
    }
    return true;
}

bool XmlToJson::isListRoot(QDomElement& rootEl)
{
    int namedAmount;
    int totalAmount;
    bool isList = false;
    if(rootEl.hasChildNodes()){
        QDomNode firstChild = rootEl.firstChild();
        QString childName = firstChild.toElement().tagName();
        namedAmount = rootEl.elementsByTagName(childName).size();
        totalAmount =  rootEl.childNodes().size();
        if(namedAmount == totalAmount )
            isList = true;
        if(namedAmount < 2)
            isList = false;
    }
#ifdef DEBUG
    qDebug()<< rootEl.tagName() << "Ended Parsing = " << (isList ? "YES" : "NO");
    qDebug() << " amount = "<<totalAmount << "  namedAmount['"<< rootEl.firstChild().nodeName() <<"'] = " <<namedAmount;
#endif
    return isList;
}


bool XmlToJson::checkFileExistance(const QString filename)
{
    QFileInfo fileinfo(filename);
    return fileinfo.exists();
}
/*
TODO: id the errors.
*/
bool XmlToJson::validateInputs()
{
    bool error = true;
    QString errorMessage;
    if(this->filename.isNull() || this->filename.isEmpty())
    {
        errorMessage.append("Input File Is Null");
        error = false;
    }
    if(this->outputRoute.isNull() || this->outputRoute.isEmpty())
    {
      errorMessage.append("Output File Is Null");
        error = false;
    }

    if(!checkFileExistance(this->filename)){
        errorMessage.append(" Input file do not exists" ) ;
        error = false;
    }
#ifdef DEBUG
    qDebug() << errorMessage;
#endif

    return error;
}




